#!/usr/bin/env bash

#-------------------------------------------------------------------------------
# Publish debian artifacts from the CERN Gitlab CI.
#-------------------------------------------------------------------------------
set -ex

EOS_CODENAME="diopside"
BUILD_TYPE="tag"
STCI_ROOT_PATH="/eos/project/s/storage-ci/www/debian"

for RELEASE in "jammy" "noble"; do
  if [ -d ./ubuntu-${RELEASE}/ ]; then
    EXPORT_REPO="${STCI_ROOT_PATH}/eos/${EOS_CODENAME}"
    mkdir -p ${EXPORT_REPO} || true
    echo "info: Publishing for: ${RELEASE} in location: ${EXPORT_REPO}"
    reprepro -Vb ${EXPORT_REPO} includedeb ${RELEASE} ./ubuntu-${RELEASE}/*.deb
  fi
done

exit 0
