#-------------------------------------------------------------------------------
# When using 'make install' with prefix option,
# the following target 'install-header_cxx' will generate
# a line which is too long, ending in the following error:
#
# > [INSTALL] Installing public C++ headers
# > make: execvp: /bin/sh: Argument list too long
# > make: *** [install-headers_cxx] Error 127
# > Makefile:3021: recipe for target 'install-headers_cxx' failed
#
# Issue tracked at:
# https://github.com/grpc/grpc/pull/14844
#-------------------------------------------------------------------------------
%define _prefix /opt/eos/grpc
%define _unpackaged_files_terminate_build 0

%if 0%{?rhel} == 7
  # CentOS 7 can use ".el7.centos" or ".el7.cern". However, we want to avoid that
  # because keeping the ".cern/centos" part will make the compilation fail
  # due to the issue described in the note above.
  %define dist .el7
%endif

#-------------------------------------------------------------------------------
# The eos-grpc package provides a series of libraries (protobuf, grpc, absl etc)
# that might clash with system provided packages. To avoid conflicts with such
# packages, we want to disable RPMS's automatic dependency processing when it
# comes to the list of capabilities the current package provides.
#-------------------------------------------------------------------------------
Autoprov: 0

#-------------------------------------------------------------------------------
# Furthermore, the capabilities provided by the package are also listed in the
# list of required capabilities, therefore, we need to exclude the particular
# libraries that this package will provide to EOS to avoid missing requirements
# when installing it.
#-------------------------------------------------------------------------------
%global __requires_exclude ^libprot.*$|^libabsl.*$|^libgrpc.*$|^libaddress_sorting.*$|^libre2.*$|^libgpr.*$|^libupb.*$

#-------------------------------------------------------------------------------
# Package definitions
#-------------------------------------------------------------------------------
Summary: gRPC, A high performance, open-source universal RPC framework
Name: eos-grpc
Version: 1.56.1
Release: 3%{?dist}%{?_with_asan:.asan}%{?_with_tsan:.tsan}
License: BSD
URL: http://www.grpc.io/
Source: https://github.com/grpc/grpc/archive/eos-grpc-%{version}.tar.gz
Obsoletes: eos-protobuf3 <= 3.17.3
Obsoletes: eos-protobuf3-compiler <= 3.17.3
Obsoletes: eos-protobuf3-devel <= 3.17.3
Obsoletes: eos-protobuf3-debuginfo <= 3.17.3
Obsoletes: eos-protobuf3-static <= 3.17.3
Obsoletes: eos-protobuf3-lite <= 3.17.3
Obsoletes: eos-protobuf3-lite-devel <= 3.17.3
Obsoletes: eos-protobuf3-lite-static <= 3.17.3
Obsoletes: eos-protobuf3-debuginfo <= 3.17.3

# For asan/tsan builds we need devtoolset-9
%if %{?_with_asan:1}%{!?_with_asan:0} || %{?_with_tsan:1}%{!?_with_tsan:0}
%define devtoolset devtoolset-9
%else
%define devtoolset devtoolset-8
%endif

# Handle the different binaries for the cmake package depending on the OS
%if 0%{?rhel} == 7
BuildRequires: %{devtoolset}-gcc-c++
BuildRequires: cmake3
%define cmake cmake3
%else
BuildRequires: cmake
%define cmake cmake
%endif

BuildRequires: sed
BuildRequires: pkgconfig gcc gcc-c++
BuildRequires: openssl-devel
BuildRequires: python3-devel python3-setuptools
BuildRequires: zlib-devel
BuildRequires: gnutls-devel
BuildRequires: libcurl-devel
BuildRequires: c-ares-devel
BuildRequires: binutils-devel
BuildRequires: libgcrypt-devel
BuildRequires: libtool

Requires: zlib
Requires: openssl
Requires: gnutls
Requires: libcurl
Requires: c-ares
Requires: binutils
Requires: libgcrypt

%if %{?_with_asan:1}%{!?_with_asan:0}
%if 0%{?rhel} == 7
BuildRequires: libasan5, %{devtoolset}-libasan-devel
Requires: libasan5
%else
BuildRequires: libasan
Requires: libasan
%endif
%endif

%if %{?_with_tsan:1}%{!?_with_tsan:0}
%if 0%{?rhel} == 7
BuildRequires: libtsan, %{devtoolset}-libtsan-devel
Requires: libtsan
%else
BuildRequires: libtsan
Requires: libtsan
%endif
%endif

%description
Remote Procedure Calls (RPCs) provide a useful abstraction for
building distributed applications and services. The libraries in this
package provide a concrete implementation of the gRPC protocol,
layered over HTTP/2. These libraries enable communication between
clients and servers using any combination of the supported languages.

%package plugins
Summary: gRPC protocol buffers compiler plugins
Requires: %{name}%{?_isa} = %{version}-%{release}

%description plugins
Plugins to the protocol buffers compiler to generate gRPC sources.

%package devel
Summary: gRPC library development files
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
Development headers and files for gRPC libraries.

%package static
Summary: gRPC library static files
Requires: %{name}-devel%{?_isa} = %{version}-%{release}

%description static
Static libraries for gRPC.

%prep
%setup -q -n eos-grpc-%{version}
# NOTE: We need to patch protobuf CMake file to properly create the symlink
# libprotobuf.so.{MINOR} -> libprotobuf.so.{MAJOR}.{MINOR}.{PATCH}
# what this means libprotobuf.so.23 -> libprotobuf.so.4.23.1
sed -i '/^set_target_properties(libprotobuf PROPERTIES/a\ \ \ \ SOVERSION ${protobuf_VERSION_MINOR}' third_party/protobuf/cmake/libprotobuf.cmake

%build
%if %{?_with_asan:1}%{!?_with_asan:0}
export CXXFLAGS='-fsanitize=address'
%endif

%if %{?_with_tsan:1}%{!?_with_tsan:0}
export CXXFLAGS='-fsanitize=thread'
%endif

%if 0%{?rhel} == 7
source /opt/rh/%{devtoolset}/enable
%endif

mkdir build
pushd build
%{cmake} ../ \
 -DCMAKE_BUILD_TYPE=RelWithDebInfo \
 -DCMAKE_INSTALL_PREFIX=%{_prefix} \
 -DCMAKE_INSTALL_RPATH=%{_prefix}/lib64 \
 -DCMAKE_SKIP_BUILD_RPATH=false \
 -DgRPC_INSTALL=ON \
 -DgRPC_INSTALL_LIBDIR=lib64 \
 -DgRPC_SSL_PROVIDER=package \
 -DgRPC_ZLIB_PROVIDER=package \
 -DgRPC_PROTOBUF_PROVIDER=module \
 -DgRPC_BUILD_CSHARP_EXT=OFF \
 -DgRPC_BUILD_GRPC_CSHARP_PLUGIN=OFF \
 -DgRPC_BUILD_GRPC_NODE_PLUGIN=OFF \
 -DgRPC_BUILD_GRPC_OBJECTIVE_PLUGIN=OFF \
 -DgRPC_BUILD_GRPC_PHP_PLUGIN=OFF \
 -DgRPC_BUILD_GRPC_RUBY_PLUGIN=OFF \
 -DBUILD_SHARED_LIBS=ON
%make_build
popd

%install
%if 0%{?rhel} == 7
source /opt/rh/%{devtoolset}/enable
%endif

export QA_RPATHS=3
pushd build
make install DESTDIR=%{buildroot} STRIP=/bin/true
popd

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%{_libdir}/*.so.*
%{_datadir}/grpc

%files plugins
%{_bindir}/*

%files devel
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
%{_includedir}/*

%files static
%{_libdir}/*.a
