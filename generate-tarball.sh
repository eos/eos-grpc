#/usr/bin/env BASH

trap 'clean_up' EXIT

clean_up()
{
  if [[ -z "${tdir}" ]]; then
    rm -rf ${tdir}
  fi
}


# Parser from: https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash
usage_error () { echo >&2 "$(basename "$0"):  $1"; exit 2; }
assert_argument () { test "$1" != "$EOL" || usage_error "$2 requires an argument"; }

tag=""
source=""

# One loop, nothing more.
if [ "$#" != 0 ]; then
  EOL=$(printf '\1\3\3\7')
  set -- "$@" "$EOL"
  while [ "$1" != "$EOL" ]; do
    opt="$1"; shift
    case "$opt" in
      # Your options go here.
      -t|--tag)    assert_argument "$1" "$opt"; tag=$1; shift;;
      -s|--source) assert_argument "$1" "$opt"; source="$1"; shift;;
      --help) usage $0; exit 0;;

      # Arguments processing. You may remove any unneeded line after the 1st.
      -|''|[!-]*) set -- "$@" "$opt";;                                          # positional argument, rotate to the end
      --*=*)      set -- "${opt%%=*}" "${opt#*=}" "$@";;                        # convert '--name=arg' to '--name' 'arg'
      -[!-]?*)    set -- "$(echo "${opt#-}" | sed 's    /g')" "$@";;            # convert '-abc' to '-a' '-b' '-c'
      --)         while [ "$1" != "$EOL" ]; do set -- "$@" "$1"; shift; done;;  # process remaining arguments as positional
      -*)         usage_error "unknown option: '$opt'";;                        # catch misspelled options
      *)          usage_error "this should NEVER happen ($opt)";;               # sanity test for previous patterns
    esac
  done
  shift  # $EOL
fi

if [[ -z "${tag}" ]] || [[ -z "${source}" ]]; then
    echo "error: tag or source directory is empty"
    exit 1
fi

echo "info: create tar archive for tag:${tag} using source:${source}"

# Remove initial v from tag name for use in filenames
if [ ${tag:0:1} = 'v' ] ; then
    ftag=${tag:1}
else
    ftag=${tag}
fi

if [ -r eos-grpc-${ftag}.tar.gz ] ; then
    echo eos-grpc-${ftag}.tar.gz already exists
    exit 1
fi


tdir=$(mktemp -d)
mkdir -p ${tdir}/eos-grpc-${ftag}
rsync -av --progress ${source}/ --exclude ".git/" --exclude ".git*" --exclude "build/" --exclude "examples/" --exclude "doc/"  ${tdir}/eos-grpc-${ftag}
tar -czvf eos-grpc-${ftag}.tar.gz -C ${tdir} ./eos-grpc-${ftag}
